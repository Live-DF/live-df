# live-df Makefile <https://debian-facile.org/projets:live-df>

all: 
	@echo "Usage: as root"
	@echo "make 586			: build live-df 586"
	@echo "make 686			: build live-df 686-pae"
	@echo "make 64			: build live-df amd64"
	@echo "make clean		: clean up build directories"
	@echo "make cleanfull	: clean up cache directories"

586:
	@echo "building live-df 586"
	# copie du script principal du build
	cp auto/config586 auto/config
	# construction du live
	lb build
	# renommer les fichiers
	mkdir -p live-df-586
	mv live-image-i386.hybrid.iso live-df-586/live-df-586.iso
	mv chroot.packages.install live-df-586/live-df-586.pkgs
	mv *.log live-df-586/live-df-586.log
	md5sum live-df-586/live-df-586.iso > live-df-586/live-df-586.md5

686:
	@echo "building live-df 686-pae"
	# copie du script principal du build
	cp auto/config686 auto/config
	# construction du live
	lb build
	# renommer les fichiers
	mkdir -p live-df-686
	mv live-image-i386.hybrid.iso live-df-686/live-df-686-pae.iso
	mv chroot.packages.install live-df-686/live-df-686-pae.pkgs
	mv *.log live-df-686/live-df-686-pae.log
	md5sum live-df-686/live-df-686-pae.iso > live-df-686/live-df-686-pae.md5
	
64:
	@echo "building live-df amd64"
	# copie du script principal du build
	cp auto/config64 auto/config
	# construction du live
	lb build
	# renommer les fichiers
	mkdir -p live-df-64
	mv live-image-amd64.hybrid.iso live-df-64/live-df-amd64.iso
	mv chroot.packages.install live-df-64/live-df-amd64.pkgs
	mv *.log live-df-64/live-df-amd64.log
	md5sum live-df-64/live-df-amd64.iso > live-df-64/live-df-amd64.md5

clean:
	@echo "cleaning build directories"
	# nettoyage du script principal du build
	rm -f auto/config
	# nettoyage du live
	lb clean

cleanfull:
	@echo "cleaning cache directories"
	rm -R -f cache

