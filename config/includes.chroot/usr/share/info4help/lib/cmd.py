#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""
Developers :                 thuban (thuban@yeuxdelibad.net)
                             arpinux (arpinux@member.fsf.org)
                             Starsheep (starsheep@openmailbox.org)
                             FirePowi
                             Michael Gebetsroither <michael@mgeb.org> (paste part)
                             
Licence :            GNU General Public Licence v3

Description :           Tool to list a description of the system
                        to improve help on forums in case of trouble.
"""


import os
import sys
import io
import subprocess
import gettext
from lib.export import export

appname="info4help"

#i18n
gettext.bindtextdomain(appname, '/usr/share/locale')
gettext.textdomain(appname)
_ = gettext.gettext


def read_as_utf8(fileno):
    fp = io.open(fileno, mode="r", encoding="utf-8", closefd=False)
    txt = fp.read()
    fp.close()
    return txt

class Cmd:
    def __init__(self,cmd,info):
        self.cmd = cmd
        self.info = info
        
    def get_info(self):
        return self.info
        
    def read(self):
        return os.popen(self.cmd).read()
        
    def read_as_root(self):
        txt = ""
        #~ p = subprocess.Popen(["pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY "+self.cmd], shell=True, stdout=subprocess.PIPE)
        p = subprocess.Popen(["pkexec "+self.cmd], shell=True, stdout=subprocess.PIPE)
        txt = read_as_utf8(p.stdout.fileno())
        return txt


class FileInfo:
    def __init__(self,name,info):
        self.name = name
        self.info = info
        self.cmd = "cat " + name
        
    def get_info(self):
        return self.info
        
    def read(self):
      if os.path.isfile(self.name):
          with open(self.name, 'r') as hi:
              return hi.read()
      else:
          return "unknown"
          
          
class DirInfo:
    def __init__(self,directory,info,ext=""):
        self.directory = directory
        self.info = info
        self.ext = ext
        self.cmd = ""
        if os.path.isfile(self.directory):
            self.cmd += "cat " + self.directory
        if os.path.isdir(self.directory+'.d'):
            if self.cmd:
                self.cmd += " && "
            self.cmd += "ls "+self.directory+'.d'+" | cat"
            
        
    def get_info(self):
        return self.info
        
    def read(self):
        info = ""
        if os.path.isfile(self.directory):
            with open(self.directory, 'r') as s:
                info = self.directory+":\n{}".format(s.read())
        if os.path.isdir(self.directory+'.d'):
            for fileinfo in os.listdir(self.directory+'.d'):
                if self.ext == "":
                    with open(os.path.join(self.directory+'.d',fileinfo), 'r') as s:
                        info += "#"+self.directory+".d/{}\n{}".format(fileinfo,s.read())
                else:
                    if fileinfo.endswith(self.ext):
                        with open(os.path.join(self.directory+'.d',fileinfo), 'r') as s:
                            info += "#"+self.directory+".d/{}\n{}".format(fileinfo,s.read())
        return info


class cmd_list:
    # Information on the system
    debver = FileInfo("/etc/debian_version",_("Your Debian version"))
    host = Cmd("uname -n",_("Name of the distribution"))
    kernel = Cmd("uname -sr",_("Kernel version"))
    sources = DirInfo('/etc/apt/sources.list',_("Package sources"),'.list')
    preferences = DirInfo('/etc/apt/preferences',_("Pinning preferences"))
    
    # Information on the hardware
    pci = Cmd("lspci",_("List of internal peripherals"))
    usb = Cmd("lsusb",_("List of connected peripherals"))
    hw = Cmd("/usr/share/info4help/scripts/dfl-dmidecode.sh",_("Hardware informations"))
    scr = Cmd("xrandr",_("Monitor configuration"))
    
    # Information on the network
    netinterfaces = FileInfo('/etc/network/interfaces',_("List of network interfaces"))
    resolvconf = FileInfo('/etc/resolv.conf',_("Configuration of the DNS resolver"))
    ifconfig = Cmd("/sbin/ifconfig -a",_("Network configuration"))
    route = Cmd("/sbin/route -n",_("IP routing table"))
    
    #information on the storage
    fs = Cmd("df -h",_("Disk use"))
    part = Cmd("/usr/share/info4help/scripts/dfl-fdisk-l.sh",_("Disk partitioning"))
    fstab = FileInfo("/etc/fstab",_("File system table"))


def get_distro(template):
    txt = export(template, cmd_list.debver.get_info(), cmd_list.debver.cmd, "user", cmd_list.debver.read())
    txt += export(template, cmd_list.host.get_info(), cmd_list.host.cmd, "user", cmd_list.host.read())
    txt += export(template, cmd_list.kernel.get_info(), cmd_list.kernel.cmd, "user", cmd_list.kernel.read())
    txt += export(template, cmd_list.sources.get_info(), cmd_list.sources.cmd, "user", cmd_list.sources.read(), "apt_sources")
    txt += export(template, cmd_list.preferences.get_info(), cmd_list.preferences.cmd, "user", cmd_list.preferences.read())
    return(txt)
 
 
def get_hw(template):
    txt = export(template, cmd_list.pci.get_info(), cmd_list.pci.cmd, "user", cmd_list.pci.read())
    txt += export(template, cmd_list.usb.get_info(), cmd_list.usb.cmd, "user", cmd_list.usb.read())
    txt += export(template, cmd_list.hw.get_info(), "/usr/sbin/dmidecode -q -t bios -t system -t processor -t memory", "root", cmd_list.hw.read_as_root())
    txt += export(template, cmd_list.scr.get_info(), cmd_list.scr.cmd, "user", cmd_list.scr.read())
    return(txt)
 
 
def get_network(template):
    txt = export(template, cmd_list.netinterfaces.get_info(), cmd_list.netinterfaces.cmd, "user", cmd_list.netinterfaces.read())
    txt += export(template, cmd_list.resolvconf.get_info(), cmd_list.resolvconf.cmd, "user", cmd_list.resolvconf.read())
    txt += export(template, cmd_list.ifconfig.get_info(), cmd_list.ifconfig.cmd, "user", cmd_list.ifconfig.read())
    txt += export(template, cmd_list.route.get_info(), cmd_list.route.cmd, "user", cmd_list.route.read())
    return(txt)
 
 
def get_disks(template):
    txt = export(template, cmd_list.fs.get_info(), cmd_list.fs.cmd, "user", cmd_list.fs.read())
    txt += export(template, cmd_list.part.get_info(), "/sbin/fdisk -l", "root", cmd_list.part.read_as_root())
    txt += export(template, cmd_list.fstab.get_info(), cmd_list.fstab.cmd, "user", cmd_list.fstab.read())
    return(txt)


def get_all(template):
    txt = get_distro(template)
    txt += get_hw(template)
    txt += get_network(template)
    txt += get_disks(template)
    return(txt)


# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
