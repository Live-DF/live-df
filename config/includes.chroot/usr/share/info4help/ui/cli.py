#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""
Developers :                 thuban (thuban@yeuxdelibad.net)
                             arpinux (arpinux@member.fsf.org)
                             Starsheep (starsheep@openmailbox.org)
                             FirePowi
                             Michael Gebetsroither <michael@mgeb.org> (paste part)
                             
Licence :            GNU General Public Licence v3

Description :           Tool to list a description of the system
                        to improve help on forums in case of trouble.
"""


import sys
import gettext
from lib.cmd import *
from lib.export import *

appname="info4help-cli"

#i18n
gettext.bindtextdomain(appname, '/usr/share/locale')
gettext.textdomain(appname)
_ = gettext.gettext


class colors:
        GREEN="\033[32m"
        RED="\033[31m"
        CYAN="\033[36m"
        YELLOW="\033[33m"
        NORMAL = '\033[0m'
        
class styles:
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'


def write_intro():
    txt1 = _("This application obtains information about your Debian GNU/Linux system to get help on forums.")
    txt2 = _('According to your choice, your session password may be asked.')
    txt3 = _('The answer is pre-formatted, you can easily copy/paste it on forums.')
    txt4 = _('You can also provide the pastebin link (if asked): it points on the report.')
    return """{}{}
{}
{}
    - {}
    - {} ☺ .{}""".format(colors.GREEN,styles.BOLD,txt1,txt2,txt3,txt4,colors.NORMAL)
 

def myinput(txt):
    if sys.version_info >= (3, 0):
        rep = input(txt)
    else:
        rep = raw_input(txt)
    return(rep)


def cli(template):
    rep, info = "", ""
    print(write_intro())
    print(colors.GREEN)
    print("\t---\n\t"+_("What information are you interested in?")+"\n\t---")
    print(colors.NORMAL)
    print("\t{}1{} : ".format(colors.YELLOW, colors.NORMAL)+_("information on the system"))
    print("\t{}2{} : ".format(colors.YELLOW, colors.NORMAL)+_("information on the hardware"))
    print("\t{}3{} : ".format(colors.YELLOW, colors.NORMAL)+_("information on the network"))
    print("\t{}4{} : ".format(colors.YELLOW, colors.NORMAL)+_("information on the storage"))
    print("\t{}5{} : ".format(colors.YELLOW, colors.NORMAL)+_("all information listed above"))
    print("\t---")
    rep = myinput("["+_("Confirm with the Enter key")+"] > ")
    
    if rep == "1":
        info = get_distro(template)
    elif rep == "2":
        info = get_hw(template)
    elif rep == "3" :
        info = get_network(template)
    elif rep == "4" :
        info = get_disks(template)
    elif rep == "5":
        info = get_all(template)
    else:
        info = _("Unknown choice")
        sys.exit()
    
    print("\n---\n{}".format(colors.CYAN)+_("Please find below the report to copy/paste on forums")+" {}(Ctrl+Shift+C){} :".format(colors.GREEN,colors.NORMAL)) 
    print(info)
    
    # upload to a paste
    topaste = ""
    topaste = myinput(_("Would you like upload the report on internet and obtain the URL address ? (on a pastebin) [y/n]")+"\n> ")
    if topaste.lower() == _("yes")[0]:
        paste_url = paste_code(info)
        if paste_url:
            print("{}".format(colors.CYAN)+_("Copy this URL address below which links to your report on internet:")+"{}".format(colors.NORMAL))
            print(paste_url)
    
    rep = myinput(_("Press Enter to exit..."))

