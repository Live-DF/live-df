#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""
Developers :                 thuban (thuban@yeuxdelibad.net)
                             arpinux (arpinux@member.fsf.org)
                             Starsheep (starsheep@openmailbox.org)
                             FirePowi
                             Michael Gebetsroither <michael@mgeb.org> (paste part)
                             
Licence :            GNU General Public Licence v3

Description :           Tool to list a description of the system
                        to improve help on forums in case of trouble.
"""


import sys
import gettext
from lib.cmd import *
from lib.export import *
import webbrowser
import tempfile

appname="info4help-web"

#i18n
gettext.bindtextdomain(appname, '/usr/share/locale')
gettext.textdomain(appname)
_ = gettext.gettext

def write_css():
    with open(sys.path[0]+"/ui/libweb/style.css", "r") as f:
        css = f.read()
    txt = '<style type="text/css">\n'+css+'</style>'
    return txt

def write_field(title,content,code=False):
    with open(sys.path[0]+"/ui/libweb/field.html", "r") as f:
        html = f.read()
    if code:
        txt = html.format(title,' name="code"',content)
    else:
        txt = html.format(title,"",content)
    return txt

def write_header(title,css,info,btn_info,btn_msg):
    with open(sys.path[0]+"/ui/libweb/header.html", "r") as f:
        html = f.read()
    txt = html.format(title,css,title,info,btn_info,btn_msg)
    return txt

def end_form():
    return """
    <input name="poster" type="hidden" value="DFrocks">        
    <input name="expire" type="hidden" value="-1">        
    <input name="lang" type="hidden" value="text">        
    <input name="private" type="hidden" value=0>        

    </form>
    """
    
def write_footer():
    return """
    </main>
    </body>
    </html>
    """

def web(template):
    # Open temporary file
    htmlfile = tempfile.mkstemp(prefix="info4help", suffix=".html")[1]
    # Get information
    distro_info = get_distro(template)
    hw_info = get_hw(template)
    nw_info = get_network(template)
    disks_info = get_disks(template)
    all_info = "\n".join([distro_info, hw_info, nw_info, disks_info])
    # information messages
    info = _("The following infomation are already formatted. All you need to do is to copy/paste in your forum message.")
    btn_info = _("You can also upload this inofmation on a pastebin by clicking on the following button:")
    btn_msg = _("Upload on paste.debian.net")
    # Create html
    html = write_header(_("Info4df report"),write_css(),info,btn_info,btn_msg)
    html += write_field(_("Installation"),distro_info)
    html += write_field(_("Hardware"),hw_info)
    html += write_field(_("Network"),nw_info)
    html += write_field(_("Storage"),disks_info)
    html += write_field(_("Complete report"),"\n".join([distro_info, hw_info, nw_info, disks_info]),True)
    html += end_form()
    html += write_footer()
    # Write html
    with open(htmlfile, "w") as out:
        out.write(html)
    webbrowser.open('file://{}'.format(htmlfile))

