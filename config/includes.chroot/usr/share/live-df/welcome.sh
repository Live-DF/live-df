#!/bin/bash
###################

# sélecteur de clavier en live
if [ -d /home/humain ]; then
    /usr/share/live-df/keyboard_selector
fi

# message d'accueil
zenity --title "Bienvenue sur Debian" --info --ok-label "Bonne découverte et @+" --text "<b>Bonjour et bienvenue sur Debian ☺</b>

Le <b>live-DF</b> est une Debian agrémentée de quelques outils facilitants, mais une <a href=\"https://www.debian.org/\">Debian</a> avant tout :)

Vous trouverez en plus d'une Debian Classique, une suite d'outils pour vous aider dans votre découverte :
 · <b><a href=\"file:///usr/share/live-df/les_cahiers_du_debutant.pdf\">les cahiers du débutant</a></b> : un manuel adapté aux nouveaux venus dans l'univers Debian GNU/Linux,
 · une <b><a href=\"file:///usr/share/live-df/fluxbox_initiation.pdf\">documentation dédiée à votre session Fluxbox</a></b> : de quoi découvrir et prendre en main votre nouvel environnement,
 · un <b>menu pré-configuré</b>, présentant les principales applications installées sur votre système,
 · une série de liens concernant les <b>logiciles libres</b>, directement accessibles depuis la section internet du menu d'applications.

<b>Live-DF</b> est une version \"allégée\" que vous pourrez compléter juste après l'installation avec
un service d'impression, la suite bureautique LibreOffice ou le client de messagerie Icedove.

Nous vous recommandons de <b>vérifier les mises à jour disponibles</b> juste après l'installation depuis un terminal administrateur.

N'hésitez pas à visiter <a href=\"https://debian-facile.org/projets:live-df\">la page du projet</a> et à vous inscrire <a href=\"https://debian-facile.org/forum.php\">sur le forum Debian-Facile</a>
afin de nous faire part de vos retours et suggestions.

Si vous désirez modifier ce système pour vos besoins personnels ou associatifs, les <a href=\"https://debian-facile.org/projets:live-df#sources\">sources</a> sont libres <a href=\"https://www.gnu.org/licenses/quick-guide-gplv3.fr.html\">-GPLv3-</a> ☺.
"

# lancement de la post-installation
if ! [ -d /home/humain ]; then
	if ! [ -e /usr/share/live-df/postinstalled ]; then
		/usr/share/live-df/postinstall-launcher.sh
	fi
fi

# suppression du lanceur de l'écran d'accueil
if [ -e /home/$USER/.config/autostart/welcome.desktop ]; then
    rm /home/$USER/.config/autostart/welcome.desktop
fi

exit 0
