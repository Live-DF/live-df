#!/bin/bash
#  live-df post-install script

# lancer en root
if [ `whoami` != root ]; then
	zenity --error --text "La post-installation demande les droits d'administration.\n Lancez ce script en \"root\"." --timeout "5"
	exit 1
fi

# besoin du réseau
/usr/share/live-df/networkcheck
if [ $? -ne 0 ]; then
	zenity --error --text "Ce script demande une connexion réseau active.\nVeuillez configurez votre connexion."
	exit 1
fi

# choix
CHOICE=`zenity --height "300" --list --checklist --title "Post-installation" --text " Vous allez compléter votre système Debian avec différents services.\n Veuillez indiquer le(s) service(s) ou logiciels à installer.\n\n !! note !!\nVous aurez besoin d'une connexion internet active." --column "installer" --column "service/logiciel" FALSE "Serveur et client d'impression +100Mo" FALSE "Suite bureautique LibreOffice +470Mo" FALSE "Client de messagerie électronique Icedove (Thunderbird) +120Mo" FALSE "Gestionnaire de bibliothèque musicale QuodLibet +23Mo" --separator "\n"`

# si le choix est vide : exit
if [ -z "$CHOICE" ]; then
	zenity --info --text "Aucune sélection - Post-installation annulée." --timeout "3"
	exit 0
fi

# choix validé : envoi du choix dans un fichier tmp
echo "$CHOICE" > /tmp/toinstall

# mise à jour des dépôts
apt-get update | zenity --progress \
    --pulsate \
    --title "Update checker" \
    --text "Mise à jour des dépôts. Veuillez patienter..." \
    --auto-close


# lecture du fichier et installation
if grep -q Serveur /tmp/toinstall; then
    apt-get install -y task-print-server system-config-printer simple-scan cups-pdf | \
        zenity --progress --pulsate \
        --text "Installation du service d'impression et de numérisation.\nVeuillez patienter...\nVous pouvez fermer cette fenêtre, un message vous informera en fin d'installation." --auto-close
    if [ -e /usr/bin/system-config-printer ]; then
        zenity --info --text "Services d'impression et de numérisation installés avec succès.\nVous pouvez brancher et configurer votre imprimante."
    else
        zenity --error --text "Oops, une erreur est survenue : venez nous voir sur le forum."
    fi
    echo "postinstalled" > /usr/share/live-df/postinstalled
fi
if grep -q LibreOffice /tmp/toinstall; then
    apt-get install -y libreoffice libreoffice-gtk libreoffice-l10n-fr | \
        zenity --progress --pulsate \
        --text "Installation de la suite bureautique LibreOffice.\nVeuillez patienter...\nVous pouvez fermer cette fenêtre, un message vous informera en fin d'installation." --auto-close
    if [ -e /usr/bin/libreoffice ]; then
        zenity --info --text "Suite bureautique Libreoffice isntallée avec succès."
    else
        zenity --error --text "Oops, une erreur est survenue : venez nous voir sur le forum."
    fi
    echo "postinstalled" > /usr/share/live-df/postinstalled
fi
if grep -q Icedove /tmp/toinstall; then
    apt-get install -y icedove icedove-l10n-fr enigmail | \
        zenity --progress --pulsate \
        --text "Installation du client de messagerie Icedove.\nVeuillez patienter...\nVous pouvez fermer cette fenêtre, un message vous informera en fin d'installation." --auto-close
    if [ -e /usr/bin/icedove ]; then
        zenity --info --text "Client de messagerie électronique Icedove installé avec succès."
    else
        zenity --error --text "Oops, une erreur est survenue : venez nous voir sur le forum."
    fi
    echo "postinstalled" > /usr/share/live-df/postinstalled
fi
if grep -q QuodLibet /tmp/toinstall; then
    apt-get install -y quodlibet sox mpg123 vorbis-tools asunder lame | \
        zenity --progress --pulsate \
        --text "Installation du gestionnaire de musique QuodLibet.\nVeuillez patienter...\nVous pouvez fermer cette fenêtre, un message vous informera en fin d'installation." --auto-close
    if [ -e /usr/bin/quodlibet ]; then
        zenity --info --text "Gestionnaire de musique QuodLibet installé avec succès."
    else
        zenity --error --text "Oops, une erreur est survenue : venez nous voir sur le forum."
    fi
    echo "postinstalled" > /usr/share/live-df/postinstalled
fi

# nettoyage
rm /tmp/toinstall

exit 0
