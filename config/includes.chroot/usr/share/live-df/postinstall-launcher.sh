#!/bin/bash
# live-df post-install script launcher


# postinstall ??
zenity --question --title "Post-installation" --text "Live-DF est une version Debian allégée :\n souhaitez-vous compléter votre installation avec le service d'impression,\n la suite bureautique LibreOffice ou encore\n le client de messagerie électronique Icedove ?\n\n !! note !!\nVous aurez besoin d'une connexion internet active.\nLe mot de passe administrateur vous sera demandé."

if [ $? = 0 ]; then
    # confirmer si déjà lancé
    if [ -e /usr/share/live-df/postinstalled ]; then
        zenity --question --title "Confirmation" --text "La post-installation a déjà été effectuée.\n Voulez-vous relancer le processus ?"
        if [ $? = 0 ]; then
            gksu /usr/share/live-df/postinstall.sh
        else
            zenity --info --text "Processus annulé" --timeout "5"
            exit 0
        fi
    else
        gksu /usr/share/live-df/postinstall.sh
    fi
else
    zenity --info --text "Processus annulé. Vous pouvez le relancer depuis\nle menu 'Outils'>'Post-installation'" --timeout "10"
    exit 0
fi

exit 0
