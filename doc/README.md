## documentation de construction du projet Live-DF

Pour toute information, visitez [la page principale du projet](https://debian-facile.org/projets:live-df) sur le portail Debian-Facile.

Vous trouverez dans ce dossier la documentation nécessaire pour contruire une ISOs basée sur Debian stable ainsi qu'une documentation spécifique pour les ISOs du projet Live-DF.
